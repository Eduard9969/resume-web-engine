<?php

    /*
     * Class assembly
     */
    require_once './vendor/autoload.php';

    /*
     * Transitions
     */
    use engine\core\Router;
    use engine\lib\TimeOut;
    use Dotenv\Dotenv;

    /*
     * DotEnv config
     */
    try {
        $dotenv = Dotenv::createImmutable(__DIR__);
        $dotenv->load();
    } catch (Exception $e) {
        header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
        exit();
    }

    /*
     * Debug options
     * To enable / disable debug functions.
     * Mode 2 to display PHP errors.
     */
    define('DEBUG_ON', env('DEBUG_ENABLE', false));
    define('DEBUG_MODE', env('DEBUG_MODE', 1));

    require_once 'engine/lib/Dev.php';

    /*
     * Run Time Fix to calculate load time
     */
    $timer = new TimeOut();

    /*
     * Basic definitions
     */
    $protocol = $_SERVER['HTTP_SCHEME'] ?? 'http' . (((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || 443 == $_SERVER['SERVER_PORT']) ? 's' : '' ) . '://';
    $host     = $_SERVER['HTTP_HOST'];

    define('URL',  $protocol . $host);
    define('DIR',  str_replace('\\', '/', __DIR__));

    /*
     * Turn on / off the stub
     */
    define('PLUG', env('PLUG_ENABLE', false));

    /*
     * Turn on / off the models
     * If not used, then the model is an empty object.
     */
    if(!defined('MODEL_ON'))
        define('MODEL_ON', env('MODEL_ENABLE', false));

    /*
     * Start session init
     */
    session_start();

    /*
     * Blade Template port
     */
    require_once 'engine/lib/assets/blade/helpers.php';

    use engine\lib\assets\blade\FileViewFinder;
    use engine\lib\assets\blade\Factory;
    use engine\lib\assets\blade\Compilers\BladeCompiler;
    use engine\lib\assets\blade\Engines\CompilerEngine;
    use engine\lib\assets\blade\Filesystem;
    use engine\lib\assets\blade\Engines\EngineResolver;

    $path       = [DIR . '/views'];         // template files
    $cachePath  = DIR . '/tmp/cache/views'; // cache template files

    if(!file_exists($cachePath)) mkdir($cachePath);

    $file       = new Filesystem();
    $compiler   = new BladeCompiler($file, $cachePath);

    /*
     * Indication of directives. Example
     */
    $compiler->directive('datetime', function($timestamp) {
        return preg_replace('/(\(\d+\))/', '<?php echo date("Y-m-d H:i:s", $1); ?>', $timestamp);
    });

    $resolver = new EngineResolver();
    $resolver->register('blade', function () use ($compiler) {
        return new CompilerEngine($compiler);
    });

    $factory = new Factory($resolver, new FileViewFinder($file, $path));
    $factory->addExtension('blade.tpl', 'blade');

    /*
     * Start tracking
     */
    $router = new Router();
    try {
        $router->run();
    } catch (Throwable $e) {
        // error handling view
        if(DEBUG_ON) dd($e);
        else {
            header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
            exit();
        }
    }

    /**
     * Get Env variable
     *
     * @param $name
     * @param null $default
     * @param bool $local_only
     * @return array|false|string|null
     */
    function env($name, $default = null, $local_only = false)
    {
        $env = getenv($name, $local_only);
        return (is_null($env) || $env === false) ? $default : $env;
    }
