<?php


namespace engine\core;

/**
 * Class Facade
 * @package engine\core
 */
abstract class Facade
{

    /**
     * Include Config Data
     *
     * @param $name
     * @return array|mixed
     */
    public function config($name)
    {
        $file = DIR . '/engine/config/' . $name . '.php';
        if(!file_exists($file)) return [];

        return require_once $file;
    }

}