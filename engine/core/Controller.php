<?php

namespace engine\core;

use engine\core\View;
use engine\lib\Lang;

/**
 * Class Controller
 * @package engine\core
 */
abstract class Controller extends Facade
{

    /**
     * @var
     */
    public $route;

    /**
     * @var \engine\core\View
     */
    public $view;

    /**
     * @var
     */
    public $acl;

    /**
     * @var array
     */
    private $langs = [];

    /**
     * @var mixed
     */
    protected $model;

    /**
     * @var null
     */
    protected $lang;

    /**
     * Controller constructor.
     *
     * @param $route
     * @throws \Throwable
     */
    public function __construct($route)
    {
        $this->route = $route;

        if(!$this->checkAcl())
            View::errorCode(403);

        $this->view  = new View($route);
        $this->lang  = $this->setLang();
        $this->model = $this->loadModel($route['controller']);
    }

    /**
     * Загрузка модели контроллера
     *
     * @param $name
     * @return mixed
     */
    public function loadModel($name)
    {
        if(!MODEL_ON)
            return new \stdClass();

        $path = 'engine\models\\' . ucfirst($name);

        if(class_exists($path))
            return new $path;
    }

    /**
     * Установка языка интерфейса
     *
     * @return null
     */
    public function setLang()
    {
        $mLang = new Lang();

        $check = $this->checkLangRequest();
        if(!empty($check))
            $this->view->redirect_previous();

        $langs = $this->config('langs');
        $mLang->setLangShortsTitle($langs);

        $request_array = $_SESSION ?? [];
        $mLang->setLangFromRequest($request_array);

        $lang = $mLang::getLang();

        /*
         * Set Language view
         */
        $this->view->assign('lang',  $lang);
        $this->view->assign('langs', $this->langs = $langs);

        return $lang;
    }

    /**
     * Получения массива языков
     *
     * @return mixed
     */
    protected function getLangs()
    {
        return $this->langs;
    }

    /**
     * Проверка изменения языка
     *
     * @return mixed|null
     */
    private function checkLangRequest()
    {
        return isset($_REQUEST['lng']) ? $_SESSION['lang'] = $_REQUEST['lng'] : null;
    }

    /**
     * Распределение ролей пользователя
     *
     * @return bool
     */
    public function checkAcl()
    {
        /*
         * Файл идентификатор доступа к контроллеру
         */
        $acl_path = 'engine/acl/' . $this->route['controller'] . '.php';
        if(!file_exists($acl_path)) return false;

        $this->acl = require_once $acl_path;

        if($this->isAcl('all')) return true;

        return false;
    }

    /**
     * Проверка роли пользователя
     *
     * @param $key
     * @return bool
     */
    public function isAcl($key)
    {
        return isset($this->acl[$key]) && in_array($this->route['action'], $this->acl[$key]);
    }

    /**
     * Проверка строки на JSON тип
     *
     * @param $string
     * @return bool
     */
    public function isJSON($string)
    {
        return ((is_string($string) && (is_object(json_decode($string)) || is_array(json_decode($string)))));
    }

    /**
     * Получение IP адреса клиента
     *
     * @return mixed
     */
    public function getIP()
    {
        return isset($_SERVER['HTTP_X_REAL_IP']) ? $_SERVER['HTTP_X_REAL_IP'] : $_SERVER['REMOTE_ADDR'];
    }

}