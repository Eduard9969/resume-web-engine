<?php

namespace engine\core;

use engine\lib\TimeOut;

/**
 * Class View
 * @package engine\core
 */
class View
{

    /**
     * @var string
     */
    public $path;

    /**
     * @var
     */
    public $route;

    /**
     * @var string
     */
    public $layout = 'default';

    /**
     * @var array
     */
    public $vars = [];

    /**
     * @var null
     */
    public $prev_url = null;

    /**
     * View constructor.
     * @param $route
     */
    public function __construct($route)
    {
        $this->route = $route;
        $this->path = $route['controller'] . '/' . $route['action'];

        if(isset($_SERVER['HTTP_REFERER']))
            $this->prev_url = $_SERVER['HTTP_REFERER'];
    }

    /**
     * Присвоение переменной
     *
     * @param $var
     * @param $content
     *
     * @return mixed
     */
    public function assign($var, $content)
    {
        return $this->vars[$var] = $content;
    }

    /**
     * Отрисовка страницы
     *
     * @param string $title
     * @throws \Throwable
     */
    public function render($title = '')
    {
        global $factory;

        $this->assign('title', $title);
        $this->assign('time_out', TimeOut::getTimeOut());

        $this->assign('controller', $this->route['controller']);
        $this->assign('action',     $this->route['action']);

        echo $factory->make($this->path, $this->vars)->render();
    }

    /**
     * Вызов обработчика ошибки
     *
     * @param $code
     * @throws \Throwable
     */
    public static function errorCode($code)
    {
        $route = ['controller' => 'errors', 'action' => $code];
        $view = new View($route);

        http_response_code($code);
        $view->render('Возникла ошибка ' . $code . '. Кто-то лажает');

        exit();
    }

    /**
     * Вызов заглушки
     *
     * @param $controller
     * @throws \Throwable
     */
    public static function enablePlug($controller)
    {
        $route = ['controller' => 'plug', 'action' => 'end'];
        $view = new View($route);

        if($controller != 'main')
            $view->redirect('/', true);

        $view->layout = 'plug';
        $view->render('THE END GAME');
        exit();
    }

    /**
     * Вызов перенаправления
     *
     * @param $url
     * @param $permanently - статус 301 при перенаправлении
     */
    public function redirect($url, $permanently = false)
    {
        if($permanently)
            header('location: ' . URL . $url, true, 301);
        else
            header('location: ' . URL . $url);

        exit();
    }

    /**
     * Вызов перенаправления на внешний url
     *
     * @param $url
     */
    public function redirect_external($url)
    {
        header('location: ' . $url);
        exit();
    }

    /**
     * Вызов перенаправления на предыдущую страницу
     */
    public function redirect_previous()
    {
        if(empty($this->prev_url))
            $this->redirect('/');

        header('location: ' . $this->prev_url);
        exit();
    }

    /**
     * Возврат Ajax ответа
     *
     * @param $status
     * @param $message
     */
    public function message($status, $message)
    {
        exit(json_encode(['status' => $status, 'message' => $message]));
    }

    /**
     * Возврат Ajax содержимого
     *
     * @param $content
     */
    public function content($content)
    {
        exit(json_encode(['content' => $content]));
    }

    /**
     * Возврат Ajax редиректа
     *
     * @param $url
     */
    public function location($url)
    {
        exit(json_encode(['url' => $url]));
    }

}
