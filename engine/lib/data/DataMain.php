<?php

namespace engine\lib\data;

class DataMain
{
    /**
     * @var null
     */
    private $lang = null;

    /**
     * @var array
     */
    private $langs = ['ru', 'en'];

    /**
     * DataMain constructor.
     * @param $lang
     */
    public function __construct($lang)
    {
        $this->lang = $lang;
    }

    /**
     * Get Content
     * @return array
     */
    public function getContent()
    {
        if(!in_array($this->lang, $this->langs))
            return [];

        $content['head_content'] = $this->getContentHeader();
        $content['main_content'] = $this->getContentMain();
        $content['side_content'] = $this->getContentSidebar();
        $content['evals']        = $this->getContentEvals();

        return $content;
    }

    /**
     * Concat Array Header Content
     * @return array
     */
    private function getContentHeader()
    {
        $content = [];

        switch ($this->lang)
        {
            case 'ru':
            {
                $content = [
                    'person_title'   => 'Самойленко Эдуард',
                    'head_title'     => 'Резюме Эдуарда Самойленко - Full Stack Developer'
                ];
                break;
            }
            case 'en':
            {
                $content = [
                    'person_title'   => 'Eduard Samoilenko',
                    'head_title'     => 'Summary of Samoilenko Eduard - Full Stack Developer'
                ];
                break;
            }
        }

        $content['person_special'] = 'Full Stack Developer';
        $content['socials'] = [
            [
                'icon'  => 'linkedin',
                'title' => 'LinkedIn',
                'url'   => '/go/linkedin'
            ],
            [
                'icon'  => 'bitbucket',
                'title' => 'BitBucket',
                'url'   => '/go/bitbucket'
            ],
            [
                'icon'  => 'github',
                'title' => 'GitHub',
                'url'   => '/go/github'
            ],
        ];

        return $content;
    }

    /**
     * Concat Array Main Content
     * @return array
     */
    private function getContentMain()
    {
        $content = [];

        switch ($this->lang)
        {
            case 'ru':
            {
                $content[] = [
                    'type'  => 'roadmap',
                    'title' => 'Опыт работы',
                    'alias' => 'experience',
                    'list'  => [
                        [
                            'time'          => '2020-2021',
                            'first_header'  => 'MakeBeCool',
                            'last_header'   => 'Full Stack Developer',
                            'description'   => 'Поддержка и разработка Shopify приложений, работа в качестве аутсорсинг\аутстаффинг сотрудника.'
                        ],
                        [
                            'time'          => '2020-2021',
                            'first_header'  => 'Foridev',
                            'last_header'   => 'Full Stack Developer',
                            'description'   => 'Работа над аутсорсинг проектами и продуктами компании'
                        ],
                        [
                            'time'          => '2018-2019',
                            'first_header'  => 'Kvadrosoft',
                            'last_header'   => 'Full Stack Developer PHP',
                            'description'   => 'Разработка и поддержка продуктов компании'
                        ],
                        [
                            'time'          => '2017-2019',
                            'first_header'  => 'Personal Employed',
                            'last_header'   => 'Back-End Engineer C++',
                            'description'   => 'Личный проект связанный с разработкой модулей для игрового движка Source на базе C++ фреймворка'
                        ],
                        [
                            'time'          => '2015-2018',
                            'first_header'  => 'Freelancer',
                            'last_header'   => 'Full Stack Developer PHP',
                            'description'   => 'Разработка проектов и функциональных частей. Использование Frameworks и СMS основанных на PHP'
                        ],
                        [
                            'time'          => '2013-2015',
                            'first_header'  => 'Freelancer',
                            'last_header'   => 'Seo Expert',
                            'description'   => 'Аудит и Продвижения веб-ресурсов в поисковых системах Google и Яндекс'
                        ],
                        [
                            'time'          => '2012-2015',
                            'first_header'  => 'Freelancer',
                            'last_header'   => 'Frond-End Developer',
                            'description'   => 'Верстка и функциональное построение клиентского интерфейса с использованием Js Framework jQuery'
                        ],
                    ]
                ];

                $content[] = [
                    'type'  => 'roadmap',
                    'title' => 'Образование',
                    'alias' => 'education',
                    'list'  => [
                        [
                            'time'          => '2018-2019',
                            'first_header'  => 'Приазовский государственный технический университет, ПГТУ',
                            'last_header'   => 'г. Мариуполь, Украина',
                            'description'   => 'Степень «Магистр», специальность «Компьютерные науки», область знаний «Информационные технологии»'
                        ],
                        [
                            'time'          => '2014-2018',
                            'first_header'  => 'Приазовский государственный технический университет, ПГТУ',
                            'last_header'   => 'г. Мариуполь, Украина',
                            'description'   => 'Степень «Бакалавр», специальность «Компьютерные науки», область знаний «Информационные технологии»'
                        ],
                        [
                            'time'          => '2003-2014',
                            'first_header'  => 'Общеобразовательная школа №33, ОШ №33',
                            'last_header'   => 'г. Мариуполь, Украина',
                            'description'   => 'Полное среднее образование, 11 классов'
                        ],
                    ]
                ];

                $content[] = [
                    'type'  => 'roadmap',
                    'title' => 'Портфолио',
                    'alias' => 'portfolio',
                    'list'  => [
                        [
                            'time'          => '2020-2021',
                            'first_header'  => 'MCom - Quick Shopping List',
                            'last_header'   => '-',
                            'description'   => 'Full Stack Developer PHP. Shopify App. Участие в стартап программе'
                        ],
                        [
                            'time'          => '2020',
                            'first_header'  => 'Consumer Financing by ChargeAfter',
                            'last_header'   => '<a href="/go/cawordpress" rel="nofollow" title="Consumer Financing by ChargeAfter" target="_blank">Wordpress Marketplace</a>',
                            'description'   => 'Full Stack Developer PHP. Woocommerce Extension'
                        ],
                        [
                            'time'          => '2020',
                            'first_header'  => 'ChargeAfter Consumer Financing',
                            'last_header'   => '<a href="/go/cabigcommerce" rel="nofollow" title="ChargeAfter Consumer Financing" target="_blank">Bigcommerce Marketplace</a>',
                            'description'   => 'Full Stack Developer PHP. Bigcommerce App'
                        ],
                        [
                            'time'          => '2020',
                            'first_header'  => 'Superfoodies - We’ve got your back',
                            'last_header'   => '<a href="/go/superfoodies" rel="nofollow" title="Superfoodies - We’ve got your back. Nutritionally" target="_blank">Superfoodies.com</a>',
                            'description'   => 'Developer PHP. Миграция данных с Magento в Shopify'
                        ],
                        [
                            'time'          => '2020',
                            'first_header'  => 'Expertspost - Experts Opinion On Important Topics',
                            'last_header'   => '<a href="/go/expertspost" rel="nofollow" title="Expertspost - Experts Opinion On Important Topics" target="_blank">Expertspost.com</a>',
                            'description'   => 'Full Stack Developer PHP'
                        ],
                        [
                            'time'          => '2020',
                            'first_header'  => 'Citys Lions ‑ Достопримечательности cвоего города',
                            'last_header'   => '<a href="/go/cityslionsrepo" rel="nofollow" title="Citys Lions GitHub Repo" target="_blank">GitHub Repo</a>, <a href="/go/cityslions" rel="nofollow" title="Citys Lions ‑ Достопримечательности твоего города" target="_blank">Citys Lions Demo</a>',
                            'description'   => 'Full Stack Developer PHP'
                        ],
                        [
                            'time'          => '2020',
                            'first_header'  => 'Foridev ‑ Selling Codes app. Shopify App',
                            'last_header'   => '<a href="/go/shopifycodesapp" rel="nofollow" title="Shopify Marketplace. Foridev ‑ Selling Codes app" target="_blank">Shopify Marketplace</a>',
                            'description'   => 'Full Stack Developer PHP. Участие в стартап программе'
                        ],
                        [
                            'time'          => '2020',
                            'first_header'  => 'Резюме Самойленко Эдуарда',
                            'last_header'   => '<a href="/go/websource" rel="nofollow" title="Резюме Самойленко Эдуарда Front-End" target="_blank">Repo Front-End</a>, <a href="/go/webengine" rel="nofollow" title="Резюме Самойленко Эдуарда Back-End" target="_blank">Repo Back-End</a>',
                            'description'   => 'Full Stack Developer PHP'
                        ],
                        [
                            'time'          => '2019',
                            'first_header'  => 'ReadyTest - Автоматизированная система тестирования',
                            'last_header'   => '<a href="/go/readytest" rel="nofollow" title="ReadyTest - Автоматизированная система тестирования" target="_blank">BitBucket Repo</a>',
                            'description'   => 'Full Stack Developer PHP. Разработка в рамках Магистерской работы. Цикл разработки - 14 дней'
                        ],
                        [
                            'time'          => '2018-2019',
                            'first_header'  => 'LiveLib — сайт о книгах, социальная сеть читателей книг',
                            'last_header'   => '<a href="/go/livelib" rel="nofollow" title="LiveLib — сайт о книгах, социальная сеть читателей книг" target="_blank">Livelib.ru</a>',
                            'description'   => 'Full Stack Developer PHP'
                        ],
                        [
                            'time'          => '2018',
                            'first_header'  => 'SeoPro - Cистема контроля и тестирования веб-ресурсов',
                            'last_header'   => '<a href="/go/seopro" rel="nofollow" title="SeoPro - Cистема контроля и тестирования веб-ресурсов" target="_blank">GitHub Repo</a>',
                            'description'   => 'Full Stack Developer PHP. Разработка в рамках Бакалаврской работы. Цикл разработки - 31 день'
                        ],
                        [
                            'time'          => '2017-2019',
                            'first_header'  => 'ZloyProject - Игровое сообщество CS:GO',
                            'last_header'   => '<a href="/go/zloyproject" rel="nofollow" title="ZloyProject - Игровое сообщество CS:GO" target="_blank">BitBucket Repo</a>',
                            'description'   => 'Full Stack Developer PHP, C++ Developer. Личная разработка'
                        ],
                        [
                            'time'          => '2017-2018',
                            'first_header'  => 'Автофреш - Магазин автозапчастей с разборки',
                            'last_header'   => '<a href="/go/autofresharchive" rel="nofollow" title="Автофреш - Магазин автозапчастей с разборки" target="_blank">Аutofresh.pro</a> (WebArchive)',
                            'description'   => 'Full Stack Developer PHP'
                        ],
                        [
                            'time'          => '2016',
                            'first_header'  => 'Vaper - Магазин электронных сигарет',
                            'last_header'   => '<a href="/go/vaper" rel="nofollow" title="Vaper - Магазин электронных сигарет" target="_blank">Source Files</a>',
                            'description'   => 'Front-End Developer. Разработка в рамках Преддипломной практики. Цикл разработки - 14 дней'
                        ],
                        [
                            'time'          => '2015-2017',
                            'first_header'  => 'RushAuto - Срочный выкуп автомобилей',
                            'last_header'   => '<a href="/go/rushautoarchive" rel="nofollow" title="RushAuto - Срочный выкуп авто" target="_blank">Rushauto.ru</a> (WebArchive)',
                            'description'   => 'SEO, Front-End Developer'
                        ],
                        [
                            'time'          => '2015-2017',
                            'first_header'  => 'NewProbeg - Корректировка спидометра авто',
                            'last_header'   => '<a href="/go/newprobeg" rel="nofollow" title="NewProbeg - Корректировка спидометра авто" target="_blank">Newprobeg.ru</a>',
                            'description'   => 'SEO, Front-End Developer'
                        ],
                        [
                            'time'          => '2014-2015',
                            'first_header'  => 'Интернет-магазин Ecotekstil.ru',
                            'last_header'   => '<a href="/go/ekotekstilarchive" rel="nofollow" title="Интернет-магазин Ecotekstil.ru - Широкий ассортимент текстиля для дома" target="_blank">Ecotekstil.ru</a> (WebArchive)',
                            'description'   => 'SEO, Front-End Developer'
                        ],
                    ]
                ];

                break;
            }
            case 'en':
            {
                $content[] = [
                    'type'  => 'roadmap',
                    'title' => 'Experience',
                    'alias' => 'experience',
                    'list'  => [
                        [
                            'time'          => '2020-2021',
                            'first_header'  => 'MakeBeCool',
                            'last_header'   => 'Full Stack Developer',
                            'description'   => 'Support and development of Shopify applications, work as an outsourcing / outstaffing employee'
                        ],
                        [
                            'time'          => '2020-2021',
                            'first_header'  => 'Foridev',
                            'last_header'   => 'Full Stack Developer',
                            'description'   => 'Work on outsourcing projects and company products'
                        ],
                        [
                            'time'          => '2018-2019',
                            'first_header'  => 'Kvadrosoft',
                            'last_header'   => 'Full Stack Developer PHP',
                            'description'   => 'Development and support of company products'
                        ],
                        [
                            'time'          => '2017-2019',
                            'first_header'  => 'Personal Employed',
                            'last_header'   => 'Back-End Engineer C++',
                            'description'   => 'A personal project related to the development of modules for the Source game engine based on the C ++ framework'
                        ],
                        [
                            'time'          => '2015-2018',
                            'first_header'  => 'Freelancer',
                            'last_header'   => 'Full Stack Developer PHP',
                            'description'   => 'Development of projects and functional parts. Using Frameworks and CMS based on PHP'
                        ],
                        [
                            'time'          => '2013-2015',
                            'first_header'  => 'Freelancer',
                            'last_header'   => 'Seo Expert',
                            'description'   => 'Audit and Promotion of web resources in the search engines Google and Yandex'
                        ],
                        [
                            'time'          => '2012-2015',
                            'first_header'  => 'Freelancer',
                            'last_header'   => 'Frond-End Developer',
                            'description'   => 'Layout and functional construction of the client interface using Js Framework jQuery'
                        ],
                    ]
                ];

                $content[] = [
                    'type'  => 'roadmap',
                    'title' => 'Education',
                    'alias' => 'education',
                    'list'  => [
                        [
                            'time'          => '2018-2019',
                            'first_header'  => 'Priazov State Technical University, PSTU',
                            'last_header'   => 'Mariupol, Ukraine',
                            'description'   => 'Degree «Master», specialty «Computer Science», area of knowledge «Information Technology»'
                        ],
                        [
                            'time'          => '2014-2018',
                            'first_header'  => 'Priazov State Technical University, PSTU',
                            'last_header'   => 'Mariupol, Ukraine',
                            'description'   => 'Degree "Bachelor" specialty «Computer Science», area of knowledge «Information Technology»'
                        ],
                        [
                            'time'          => '2003-2014',
                            'first_header'  => 'Secondary school №33, school №33',
                            'last_header'   => 'Mariupol, Ukraine',
                            'description'   => 'Full secondary education, 11 classes'
                        ],
                    ]
                ];

                $content[] = [
                    'type'  => 'roadmap',
                    'title' => 'Portfolio',
                    'alias' => 'portfolio',
                    'list'  => [
                        [
                            'time'          => '2020-2021',
                            'first_header'  => 'MCom - Quick Shopping List',
                            'last_header'   => '-',
                            'description'   => 'Full Stack Developer PHP. Shopify App. Participation in a startup program'
                        ],
                        [
                            'time'          => '2020',
                            'first_header'  => 'Consumer Financing by ChargeAfter',
                            'last_header'   => '<a href="/go/cawordpress" rel="nofollow" title="Consumer Financing by ChargeAfter" target="_blank">Wordpress Marketplace</a>',
                            'description'   => 'Full Stack Developer PHP. Woocommerce Extension'
                        ],
                        [
                            'time'          => '2020',
                            'first_header'  => 'ChargeAfter Consumer Financing',
                            'last_header'   => '<a href="/go/cabigcommerce" rel="nofollow" title="ChargeAfter Consumer Financing" target="_blank">Bigcommerce Marketplace</a>',
                            'description'   => 'Full Stack Developer PHP. Bigcommerce App'
                        ],
                        [
                            'time'          => '2020',
                            'first_header'  => 'Superfoodies - We’ve got your back',
                            'last_header'   => '<a href="/go/superfoodies" rel="nofollow" title="Superfoodies - We’ve got your back. Nutritionally" target="_blank">Superfoodies.com</a>',
                            'description'   => 'Developer PHP. Migrating data from Magento to Shopify'
                        ],
                        [
                            'time'          => '2020',
                            'first_header'  => 'Expertspost - Experts Opinion On Important Topics',
                            'last_header'   => '<a href="/go/expertspost" rel="nofollow" title="Expertspost - Experts Opinion On Important Topics" target="_blank">Expertspost.com</a>',
                            'description'   => 'Full Stack Developer PHP'
                        ],
                        [
                            'time'          => '2020',
                            'first_header'  => 'Citys Lions ‑ Lions of your city',
                            'last_header'   => '<a href="/go/cityslionsrepo" rel="nofollow" title="Citys Lions GitHub Repo" target="_blank">GitHub Repo</a>, <a href="/go/cityslions" rel="nofollow" title="Citys Lions ‑ Достопримечательности твоего города" target="_blank">Citys Lions Demo</a>',
                            'description'   => 'Full Stack Developer PHP'
                        ],
                        [
                            'time'          => '2020',
                            'first_header'  => 'Foridev ‑ Selling Codes app',
                            'last_header'   => '<a href="/go/shopifycodesapp" rel="nofollow" title="Shopify Marketplace. Foridev ‑ Selling Codes app" target="_blank">Shopify Marketplace</a>',
                            'description'   => 'Full Stack Developer PHP. Shopify App. Participation in a startup program'
                        ],
                        [
                            'time'          => '2020',
                            'first_header'  => 'Summary of Samoilenko Eduard',
                            'last_header'   => '<a href="/go/websource" rel="nofollow" title="Summary of Samoilenko Eduard Front-End" target="_blank">Repo Front-End</a>, <a href="/go/webengine" rel="nofollow" title="Summary of Samoilenko Eduard Back-End" target="_blank">Repo Back-End</a>',
                            'description'   => 'Full Stack Developer PHP'
                        ],
                        [
                            'time'          => '2019',
                            'first_header'  => 'ReadyTest - Automated Testing System',
                            'last_header'   => '<a href="/go/readytest" rel="nofollow" title="ReadyTest - Automated Testing System" target="_blank">BitBucket Repo</a>',
                            'description'   => 'Full Stack Developer PHP. Development as part of the Master\'s work. Development cycle - 14 days'
                        ],
                        [
                            'time'          => '2018-2019',
                            'first_header'  => 'LiveLib - a site about books, a social network of readers of books',
                            'last_header'   => '<a href="/go/livelib" rel="nofollow" title="LiveLib - a site about books, a social network of readers of books" target="_blank">Livelib.ru</a>',
                            'description'   => 'Full Stack Developer PHP'
                        ],
                        [
                            'time'          => '2018',
                            'first_header'  => 'SeoPro - Web Resources Monitoring and Testing System',
                            'last_header'   => '<a href="/go/seopro" rel="nofollow" title="SeoPro - Web Resources Monitoring and Testing System" target="_blank">GitHub Repo</a>',
                            'description'   => 'Full Stack Developer PHP. Development as part of the Bachelor\'s work. Development cycle - 31 days'
                        ],
                        [
                            'time'          => '2017-2019',
                            'first_header'  => 'ZloyProject - CS: GO Gaming Community',
                            'last_header'   => '<a href="/go/zloyproject" rel="nofollow" title="ZloyProject - CS: GO Gaming Community" target="_blank">BitBucket Repo</a>',
                            'description'   => 'Full Stack Developer PHP, C++ Developer. Personal development'
                        ],
                        [
                            'time'          => '2017-2018',
                            'first_header'  => 'Autofresh - Auto parts store with disassembly',
                            'last_header'   => '<a href="/go/autofresharchive" rel="nofollow" title="Autofresh - Auto parts store with disassembly" target="_blank">Аutofresh.pro</a> (WebArchive)',
                            'description'   => 'Full Stack Developer PHP'
                        ],
                        [
                            'time'          => '2016',
                            'first_header'  => 'Vaper - Electronic Cigarette Shop',
                            'last_header'   => '<a href="/go/vaper" rel="nofollow" title="Vaper - Electronic Cigarette Shop" target="_blank">Source Files</a>',
                            'description'   => 'Front-End Developer. Development in the framework of undergraduate practice. Development cycle - 14 days'
                        ],
                        [
                            'time'          => '2015-2017',
                            'first_header'  => 'RushAuto - Urgent repayment of cars',
                            'last_header'   => '<a href="/go/rushautoarchive" rel="nofollow" title="RushAuto - Urgent repayment of cars" target="_blank">Rushauto.ru</a> (WebArchive)',
                            'description'   => 'SEO, Front-End Developer'
                        ],
                        [
                            'time'          => '2015-2017',
                            'first_header'  => 'NewProbeg - Auto Speedometer Adjustment',
                            'last_header'   => '<a href="/go/newprobeg" rel="nofollow" title="NewProbeg - Auto Speedometer Adjustment" target="_blank">Newprobeg.ru</a>',
                            'description'   => 'SEO, Front-End Developer'
                        ],
                        [
                            'time'          => '2014-2015',
                            'first_header'  => 'Online store Ecotekstil.ru',
                            'last_header'   => '<a href="/go/ekotekstilarchive" rel="nofollow" title="Online store Ecotekstil.ru" target="_blank">Ecotekstil.ru</a> (WebArchive)',
                            'description'   => 'SEO, Front-End Developer'
                        ],
                    ]
                ];

                break;
            }
        }

        return $content;
    }

    /**
     * Concat Array Sidebar Content
     * @return array
     */
    private function getContentSidebar()
    {
        $content = [];

        /*
         * Contacts List
         */
        $contact_list = [
            [
                'icon'          => 'phone',
                'description'   => '<a href="tel:380982015696" style="color: white" rel="nofollow">+380982015696</a>'
            ],
            [
                'icon'          => 'telegram',
                'description'   => '<a href="/go/telegram" style="color: white" rel="nofollow" target="_blank">@makkintosh</a>'
            ],
            [
                'icon'          => 'email',
                'description'   => '<a href="mailto:eduard9969@gmail.com" style="color: white" rel="nofollow">Eduard9969@gmail.com</a>'
            ],
        ];

        /*
         * Skills List
         */
        $skills_list = [
            [
                'title' => 'PHP',
                'mark'  => '4'
            ],
            [
                'title' => 'MySQL',
                'mark'  => '4'
            ],
            [
                'title' => 'Memcached',
                'mark'  => '4'
            ],
            [
                'title' => 'HTML',
                'mark'  => '4'
            ],
            [
                'title' => 'HTML5',
                'mark'  => '4'
            ],
            [
                'title' => 'CSS',
                'mark'  => '4'
            ],
            [
                'title' => 'CSS3',
                'mark'  => '4'
            ],
            [
                'title' => 'JavaScript',
                'mark'  => '3'
            ],
            [
                'title' => 'Sphinx',
                'mark'  => '3'
            ],
            [
                'title' => 'Redis',
                'mark'  => '3'
            ],
            [
                'title' => 'SQLite',
                'mark'  => '3'
            ],
            [
                'title' => 'MS SQL',
                'mark'  => '3'
            ],
            [
                'title' => 'MongoDB',
                'mark'  => '3'
            ],
            [
                'title' => 'Python',
                'mark'  => '2'
            ],
            [
                'title' => 'C++',
                'mark'  => '2'
            ],
            [
                'title' => 'C#',
                'mark'  => '2'
            ],
            [
                'title' => 'Kotlin',
                'mark'  => '2'
            ]
        ];

        /*
         * Soft Skills List
         */
        $soft_skills_list = [
            [
                'title' => 'Windows',
                'mark'  => '4'
            ],
            [
                'title' => 'Linux',
                'mark'  => '4'
            ],
            [
                'title' => 'Php/Web Storm',
                'mark'  => '4'
            ],
            [
                'title' => 'Git',
                'mark'  => '3'
            ],
            [
                'title' => 'Postman',
                'mark'  => '3'
            ],
            [
                'title' => 'Photoshop',
                'mark'  => '3'
            ],
            [
                'title' => 'Illustrator',
                'mark'  => '2'
            ],
            [
                'title' => 'Figma',
                'mark'  => '2'
            ],
        ];

        /*
         * Frameworks & Cms List
         */
        $frameworks_cms_list = [
            [
                'title' => 'Laravel',
                'mark'  => '4'
            ],
            [
                'title' => 'Wordpress',
                'mark'  => '3'
            ],
            [
                'title' => 'OpenCart',
                'mark'  => '3'
            ],
            [
                'title' => 'Drupal',
                'mark'  => '3'
            ],
            [
                'title' => 'DLE',
                'mark'  => '3'
            ],
            [
                'title' => 'Shopware',
                'mark'  => '3'
            ],
            [
                'title' => 'Symfony 2',
                'mark'  => '3'
            ],
            [
                'title' => 'jQuery',
                'mark'  => '3'
            ],
            [
                'title' => 'ReactJs',
                'mark'  => '3'
            ],
            [
                'title' => 'Lumen',
                'mark'  => '2'
            ],
            [
                'title' => '1C - Битрикс',
                'mark'  => '2'
            ],
            [
                'title' => 'Joomla',
                'mark'  => '2'
            ],
            [
                'title' => 'Angular',
                'mark'  => '2'
            ],
            [
                'title' => 'ASP.NET',
                'mark'  => '2'
            ],
        ];

        /*
         * Api List
         */
        $api_list = [
            [
                'title' => 'Google',
                'mark'  => '4'
            ],
            [
                'title' => 'Telegram',
                'mark'  => '4'
            ],
            [
                'title' => 'Vk',
                'mark'  => '4'
            ],
            [
                'title' => 'BitBucket',
                'mark'  => '4'
            ],
            [
                'title' => 'GitHub',
                'mark'  => '4'
            ],
            [
                'title' => 'GitLab',
                'mark'  => '4'
            ],
            [
                'title' => 'GTmetrix',
                'mark'  => '4'
            ],
            [
                'title' => 'Shopify',
                'mark'  => '4'
            ],
            [
                'title' => 'Bigcommerce',
                'mark'  => '3'
            ],
            [
                'title' => 'ChargeAfter',
                'mark'  => '3'
            ],
            [
                'title' => 'Vultr',
                'mark'  => '3'
            ],
            [
                'title' => 'Maxmind',
                'mark'  => '3'
            ],
            [
                'title' => 'Discord',
                'mark'  => '3'
            ],
            [
                'title' => 'Facebook',
                'mark'  => '3'
            ],
            [
                'title' => 'Twitter',
                'mark'  => '3'
            ],
        ];

        /*
         * Languages List
         */
        $langs_list = [
            [
                'title'      => 'English',
                'mark-text'  => 'Elementary, A2'
            ],
            [
                'title'      => 'Русский',
                'mark-text'  => 'Родной язык'
            ],
            [
                'title'      => 'Український',
                'mark-text'  => 'Рiдна мова'
            ],
        ];

        switch ($this->lang)
        {
            case 'ru':
            {
                $content[] = [
                    'type'  => 'text',
                    'title' => 'Обо мне',
                    'list'  => [
                        [
                            'description'   => 'Я веб-разработчик с опытом работы более 7 лет. В настоящее время я специализируюсь на разработке с применением PHP. Для решения поставленных задач я выбираю лучшие практики из своего опыта и гибкие подходы к разработке. Открыт к новым знаниям и совершенствованию своих профессиональных навыков.'
                        ],
                    ]
                ];

                array_unshift($contact_list, [
                    'icon'          => 'location',
                    'description'   => '<a href="/go/map" style="color:white" rel="nofollow" target="_blank">г. Мариуполь, Украина</a>'
                ]);

                $content[] = [
                    'type'  => 'icon',
                    'title' => 'Связь со мной',
                    'list'  => $contact_list
                ];

                $content[] = [
                    'type'  => 'eval',
                    'title' => 'Навыки',
                    'list'  => $skills_list
                ];

                break;
            }
            case 'en':
            {
                $content[] = [
                    'type'  => 'text',
                    'title' => 'About Me',
                    'list'  => [
                        [
                            'description'   => 'I am a web developer with over 7 years experience. I am currently specializing in PHP development. To solve the tasks I choose the best practices from my experience and flexible development approaches. Open to new knowledge and improve their professional skills.'
                        ],
                    ]
                ];

                array_unshift($contact_list, [
                    'icon'          => 'location',
                    'description'   => '<a href="/go/map" style="color:white" rel="nofollow" target="_blank">Mariupol, Ukraine</a>'
                ]);

                $content[] = [
                    'type'  => 'icon',
                    'title' => 'Contacts',
                    'list'  => $contact_list
                ];

                $content[] = [
                    'type'  => 'eval',
                    'title' => 'Skills',
                    'list'  => $skills_list
                ];

                break;
            }
        }

        $content[] = [
            'type'  => 'eval',
            'title' => 'Soft Skills',
            'list'  => $soft_skills_list
        ];

        $content[] = [
            'type'  => 'eval',
            'title' => 'Frameworks & CMS',
            'list'  => $frameworks_cms_list
        ];

        $content[] = [
            'type'  => 'eval',
            'title' => 'API',
            'list'  => $api_list
        ];

//        $content[] = [
//            'type'  => 'eval-text',
//            'title' => 'Языки',
//            'list'  => $langs_list
//        ];

        return $content;
    }

    /**
     * Get Evals Content
     * @return array
     */
    private function getContentEvals()
    {
        $content = [];

        switch ($this->lang)
        {
            case 'ru':
            {
                $content[] = [
                    'type'  => 'eval-mark',
                    'title' => 'Оценка',
                    'list'  => [
                        [
                            'title' => 'Базовое знание',
                            'mark'  => 1
                        ],
                        [
                            'title' => 'Удовлетворительно',
                            'mark'  => 2
                        ],
                        [
                            'title' => 'Хорошо',
                            'mark'  => 3
                        ],
                        [
                            'title' => 'Очень хорошо',
                            'mark'  => 4
                        ],
                        [
                            'title' => 'Превосходно',
                            'mark'  => 5
                        ],
                    ]
                ];
                break;
            }
            case 'en':
            {
                $content[] = [
                    'type'  => 'eval-mark',
                    'title' => 'Assessment',
                    'list'  => [
                        [
                            'title' => 'Basic knowledge',
                            'mark'  => 1
                        ],
                        [
                            'title' => 'Satisfactorily',
                            'mark'  => 2
                        ],
                        [
                            'title' => 'Good',
                            'mark'  => 3
                        ],
                        [
                            'title' => 'Very well',
                            'mark'  => 4
                        ],
                        [
                            'title' => 'Fine',
                            'mark'  => 5
                        ],
                    ]
                ];
                break;
            }
        }

        return $content;
    }

}