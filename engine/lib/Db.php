<?php

  namespace engine\lib;

  use PDO;

  class Db
  {

    protected $db;

    public function __construct()
    {
      $config = require 'engine/config/db.php';

      try
      {
          $dsn  = 'mysql:host=' . $config['host'] . ';dbname=' . $config['name'] . ';charset=' . $config['charset'];
          $user = $config['user'];
          $pass = $config['password'];

          $this->db = new PDO($dsn, $user, $pass);
      }
      catch (PDOException $e)
      {
          die('Подключение не удалось: ' . $e->getMessage());
      }
    }

    /**
    * Выполнения запроса
    *
    * @param $sql
    * @param array $params
    * @return bool|\PDOStatement
    */
    public function query($sql, $params = [])
    {
  		$stmt = $this->db->prepare($sql);

  		if (!empty($params))
  			foreach ($params as $key => $val)
  				$stmt->bindValue(':'.$key, $val);

  		return $stmt->execute();
  	}

    /**
    * Выполнение множественного запроса
    *
    * @param $sql
    * @param array $params
    * @return array
    */
  	public function row($sql, $params = [])
    {
  		$result = $this->query($sql, $params);
  		return $result->fetchAll(PDO::FETCH_ASSOC);
  	}

    /**
    * Выполнения запроса по ячейкам
    *
    * @param $sql
    * @param array $params
    * @return mixed
    */
  	public function column($sql, $params = [])
    {
  		$result = $this->query($sql, $params);
  		return $result->fetchColumn();
  	}

    /**
    * Получения последнего ID по выполнению INSERT
    *
    * @return string
    */
    public function lastInsertId()
    {
        return $this->db->lastInsertId();
    }

  }
