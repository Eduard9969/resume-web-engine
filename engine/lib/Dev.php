<?php

use Symfony\Component\VarDumper\VarDumper;

/**
 * DEBUG
 */
if (defined('DEBUG_ON') && DEBUG_ON)
{

    /*
     * Debug Mode
     */
    if (DEBUG_MODE == 2)
    {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
    }

    /*
     * DD Method
     */
    if (!function_exists('dd')) {
        function dd($var)
        {
            foreach (func_get_args() as $var) {
                VarDumper::dump($var);
            }
            exit;
        }
    }

}
