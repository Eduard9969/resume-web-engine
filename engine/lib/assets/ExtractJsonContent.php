<?php

namespace engine\lib\assets;

/**
 * Class ExtractJsonContent
 */
Class ExtractJsonContent
{

    /**
     * @var string
     */
    public $root_path     = 'template';

    /**
     * @var bool
     */
    public $enable_header = false;

    /**
     * @var null
     */
    private $content_name = null;

    /**
     * @var null
     */
    private $content_name_default = null;

    /**
     * ExtractJsonContent constructor.
     * @param $content_name
     */
    public function __construct($content_name)
    {
        $this->content_name = $content_name;
    }

    /**
     * Get Json Content
     * @return array
     */
    public function get_content()
    {
        $content = $this->get_json_file();

        return $content;
    }

    /**
     * Get Json File
     * @return array
     */
    private function get_json_file()
    {
        $path = $this->root_path . '/data/' . $this->content_name . '.json';

        if(!file_exists($path))
        {
            if($this->content_name == $this->content_name_default || empty($this->content_name_default))
                return [];

            /*
             * Clone this class for default content
             */
            $mClone = clone $this;
            $mClone->content_name = $this->content_name_default;
            return $mClone->get_content();
        }

        if($this->enable_header)
            header('Access-Control-Allow-Origin: *');

        $json = file_get_contents($path);
        if(empty($json)) return [];

        return json_decode($json, true);
    }

    /**
     * Set Default Json File
     *
     * @param $content_name
     * @return mixed
     */
    public function set_default_json_file($content_name)
    {
        return $this->content_name_default = $content_name;
    }
}