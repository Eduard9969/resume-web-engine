<?php


namespace engine\lib;

/**
 * Class Lang
 * @package engine\lib
 */
class Lang
{
    /**
     * @var null Текущий язык
     */
    private static $lang = null;

    /**
     * @var array Массив сокращений языков
     */
    private $lang_shorts = [];

    /**
     * @var array Массив языков
     */
    private $langs = [];

    /**
     * Lang constructor.
     */
    public function __construct() { }

    /**
     * Установка допустимого массива сокращений языков
     *
     * @param $array [[alias => title], [alias => title]]
     * @return mixed
     */
    public function setLangShortsTitle($array)
    {
        foreach ($array as $key => $value)
            if(is_string($key)) $this->lang_shorts[] = $key;

        return !empty($this->lang_shorts) ? $this->langs = $array : null;
    }

    /**
     * Установка текущего языка
     *
     * @param $lang_short
     * @return null
     */
    public function setLang($lang_short)
    {
        return in_array($lang_short, $this->lang_shorts) ? (self::$lang = $lang_short) : $this->setDefaultLang();
    }

    /**
     * Установка языка по-умолчанию
     * @return mixed|null
     */
    private function setDefaultLang()
    {
        return (isset($this->lang_shorts[0])) ? $this->setLang($this->lang_shorts[0]) : null;
    }

    public function setLangFromRequest($request)
    {
        $lang = (isset($request['lang']) || !empty($request['lang'])) ? $request['lang'] : $this->setDefaultLang();

        return $this->setLang($lang);
    }

    /**
     * Возвращает текуший язык
     * @return null
     */
    public static function getLang()
    {
        return self::$lang;
    }

}