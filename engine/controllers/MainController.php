<?php

  namespace engine\controllers;

  use engine\core\Controller;
  use engine\lib\assets\ExtractJsonContent;

  /**
 * Class MainController
 * @package engine\controllers
 */
class MainController extends Controller
{

    /**
    * Index Method
    */
    public function indexAction()
    {
        $oExtract = new ExtractJsonContent('main-data-' . $this->lang);
        $oExtract->root_path = DIR . '/' . $oExtract->root_path;

        $content = $oExtract->get_content();
        $title   = isset($content['head_content']['head_title']) ?
                        $content['head_content']['head_title'] : '';

        $this->view->assign('data', $content);
        $this->view->render($title);
    }

}