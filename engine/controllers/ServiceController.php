<?php

namespace engine\controllers;

use engine\core\Controller;

/**
 * Class ServiceController
 * @package engine\controllers
 */
class ServiceController extends Controller
{

    /**
     * Go Method
     */
    public function goAction()
    {
        $alias      = $this->route['alias'] ?? null;
        $url_assoc  = $this->config('go_list');

        if(empty($alias) || !key_exists($alias, $url_assoc))
            $this->view->redirect('/');

        $this->view->redirect_external($url_assoc[$alias]);
    }

}