<?php

namespace engine\controllers;

use engine\core\Controller;
use engine\lib\data\DataMain;
use engine\lib\Lang;

Class GenerateController extends Controller
{

    /**
     * Const Auth Pass
     */
    const PASS_AUTH = 'jsoncode';

    /**
     * Gemerate Method
     * @throws \Throwable
     */
    public function indexAction()
    {
        $this->lock();

        $langs = $this->getLangs();
        if(empty($langs)) exit('empty languages array');

        $file_name = 'main-data';
        foreach ($langs as $alias => $item)
            $this->create_json_file($this->generate_content($alias), $file_name . '-' . $alias);

        exit('ok');
    }

    /**
     * Lock Method
     * @throws \Throwable
     */
    private function lock()
    {
        if(!empty($_REQUEST) && isset($_REQUEST['pass']))
        {
            if($_REQUEST['pass'] == self::PASS_AUTH)
                $_SESSION['lock_on'] = true;
        }

        if(empty($_SESSION['lock_on']))
        {
            $this->view->render('Авторизация');
            exit();
        }

        return true;
    }

    /**
     * Generate Json Content. Lang version
     *
     * @param $lang
     * @return false|string
     */
    private function generate_content($lang)
    {
        return json_encode((new DataMain($lang))->getContent());
    }

    /**
     * Create json file
     *
     * @param string $content
     * @param string $file_name
     *
     * @return bool
     */
    private function create_json_file($content, $file_name = 'main-data')
    {
        $file = $file_name . '.json';
        $dir  = DIR . '/template/data/';

        if(!file_exists($dir))
            return false;

        $handle = fopen($dir . $file, 'w+');
        $write  = fwrite($handle, $content);
        fclose($handle);

        return is_int($write);
    }

}

