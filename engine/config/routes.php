<?php

  /*
   * {int:\d+}
   * {string:\w+}
   */

  return [
      '' => [
          'controller' => 'main',
          'action'     => 'index',
      ],
      'generate/json' => [
          'controller' => 'generate',
          'action'     => 'index',
      ],
      'go/{alias:\w+}' => [
          'controller' => 'service',
          'action'     => 'go',
      ]
  ];
