<?php
    return [
        'map'       => 'https://goo.gl/maps/Vcu2DfwgZexDEYuSA',
        'telegram'  => 'https://t.me/makkintosh',

        'linkedin'  => 'https://www.linkedin.com/in/eduard9969/',
        'bitbucket' => 'https://bitbucket.org/Eduard9969',
        'github'    => 'https://github.com/Eduard9969',

        'msalama'   => 'https://www.freepik.com/m-salama',
        'websource' => 'https://bitbucket.org/Eduard9969/resume-web-template/src/master/',
        'webengine' => 'https://bitbucket.org/Eduard9969/resume-web-engine/src/master/',

        'ekotekstil'        => 'http://ekotekstil.ru',
        'ekotekstilarchive' => 'https://web.archive.org/web/20180824184534/http://ecotekstil.ru/',
        'newprobeg'         => 'http://newprobeg.ru/',
        'vaper'             => 'https://1drv.ms/u/s!Aumf7D09OK5Lg6xxkFex72UfV6Vaug?e=1E59yr',
        'rushauto'          => 'http://rushauto.ru/',
        'rushautoarchive'   => 'https://web.archive.org/web/20190406032216/http://rushauto.ru/',
        'autofresh'         => 'http://autofresh.pro/',
        'autofresharchive'  => 'http://web.archive.org/web/20180717171927/http://autofresh.pro/',
        'zloyproject'       => 'https://bitbucket.org/Eduard9969/zloyprojectcaps/src/master/',
        'seopro'            => 'https://github.com/Eduard9969/seopro',
        'readytest'         => 'https://bitbucket.org/Eduard9969/readitest/src/master/',
        'livelib'           => 'https://www.livelib.ru/',
        'shopifycodesapp'   => 'https://apps.shopify.com/foridev-selling-codes-app',
        'cityslionsrepo'    => 'https://github.com/Eduard9969/citys-lions-laravel',
        'cityslions'        => 'http://lions.readytest.tk/',
        'superfoodies'      => 'https://superfoodies.com/',
        'expertspost'       => 'https://expertspost.com/',
        'cabigcommerce'     => 'https://www.bigcommerce.com/apps/chargeafter-consumer-financing/',
        'cawordpress'       => 'https://wordpress.org/plugins/consumer-financing-by-chargeafter/',
    ];