@extends('layouts.default')

@section('content')
    @include('include.part.header')


    <main class="main-content">
        <div class="main-content-wrap d-flex route-element">
            <div class="main-content-block">
                @if(isset($data['main_content']) && !empty($data['main_content']))
                    @foreach($data['main_content'] as $content)
                        @if(isset($content['type']) && isset($content['list']))
                            <section @if(isset($content['alias'])) id="{{ $content['alias'] }}" @endif>
                                @include('include.control.control-head-text', ['head_title' => $content['title']])

                                @includeIf('include.list.list-' . $content['type'], ['items' => $content['list']])
                            </section>
                        @endif
                    @endforeach
                @endif

                @include ('include.part.copyright')
            </div>

            <div class="sidebar-content-block">
                @if(isset($data['side_content']) && !empty($data['side_content']))
                    @foreach($data['side_content'] as $content)
                        @if(isset($content['type']) && isset($content['list']))
                            <section @if(isset($content['alias'])) id="{{ $content['alias'] }}" @endif>
                                @include('include.control.control-head-text', ['head_title' => $content['title'], 'without_fairy' => true])

                                @if($content['type'] == 'eval')
                                    @if(isset($data['evals']) && isset($data['evals'][0]))
                                        <noindex>
                                            @include('include.part.tooltip', ['title' => $data['evals'][0]['title'], 'evals' => $data['evals'][0]['list']])
                                        </noindex>
                                    @endif
                                @endif
                                @includeIf('include.list.list-' . $content['type'], ['items' => $content['list']])
                            </section>
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
    </main>
@endsection