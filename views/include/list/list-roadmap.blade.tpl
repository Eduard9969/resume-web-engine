@if(!empty($items))
    <div class="list-exp">
        @foreach($items as $item)
            <div class="item-roadmap-wrap">
                <div class="item-roadmap-left-block d-inline-block align-top">
                    <span>{{ $item['time'] }}</span>
                </div>
                <div class="item-roadmap-right-block d-inline-block align-top">
                    <div class="item-roadmap-right-block-header">
                        <span>{{ $item['first_header'] }}</span>
                        <span>{!! $item['last_header'] !!}</span>
                    </div>
                    <div class="item-roadmap-right-block-descpt">
                        <p>{{ $item['description'] }}</p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endif