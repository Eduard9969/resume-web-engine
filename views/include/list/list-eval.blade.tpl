@if(!empty($items))
    <ul>
        @foreach($items as $item)
        <li>
            <div class="item-with-eval-element">
                <span class="d-inline-block align-middle">{{ $item['title'] }}</span>

                <span class="apply-mark apply-mark-{{ $item['mark'] }} d-inline-block align-middle">
                    <span class="d-inline-block align-middle item-with-eval-element-mark"></span>
                    <span class="d-inline-block align-middle item-with-eval-element-mark"></span>
                    <span class="d-inline-block align-middle item-with-eval-element-mark"></span>
                    <span class="d-inline-block align-middle item-with-eval-element-mark"></span>
                    <span class="d-inline-block align-middle item-with-eval-element-mark"></span>
                </span>
                <span class="clear"></span>
            </div>
        </li>
        @endforeach
    </ul>
@endif