@if(!empty($items))
    <ul>
        @foreach($items as $item)
        <li>
            <div class="item-with-eval-element">
                <span class="d-inline-block align-middle">{{ $item['title'] }}</span>
                <span class="mark-text d-inline-block align-middle">{{ $item['mark-text'] }}</span>
                <span class="clear"></span>
            </div>
        </li>
        @endforeach
    </ul>
@endif