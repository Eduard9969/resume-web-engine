@if(!empty($items))
    <ul>
        @foreach($items as $item)
            <li>
                <div class="item-with-icon-element">
                    <svg class="svg-img d-inline-block align-middle">
                        <use xlink:href="/dist/images/svg/sprite.svg#{{ $item['icon'] }}"></use>
                    </svg><span class="d-inline-block align-middle">{!! $item['description'] !!}</span>
                </div>
            </li>
        @endforeach
    </ul>
@endif