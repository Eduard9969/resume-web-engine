@if(isset($head_title))
    <div class="head-text-block @if(empty($without_fairy))fairy_line @endif">
        <h3>
            <span class="head-text-content text-uppercase">
                {{ $head_title }}
            </span>
        </h3>
    </div>
@endif