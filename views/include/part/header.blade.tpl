<header class="wrappen-header d-flex route-element">
    <div class="wrappen-header-block main-content-block d-flex">
        <div class="wrappen-header-block-text">
            <h1 class="text-uppercase">{{ $data['head_content']['person_title'] }}</h1>
            <span>{{ $data['head_content']['person_special'] }}</span>
        </div>
        @if(isset($data['head_content']['socials']))
        <div class="wrappen-header-block-social">
            <ul>
                @foreach($data['head_content']['socials'] as $social)
                    <li class="d-inline-block align-middle">
                        <a class="social-element d-block" href="{{ $social['url'] }}" title="{{ $social['title'] }}" rel="nofollow" target="_blank">
                            <svg class="svg-img d-inline-block align-middle">
                                <use xlink:href="/dist/images/svg/sprite.svg#{{ $social['icon'] }}"></use>
                            </svg>
                            <span class="d-inline-block align-middle">{{ $social['title'] }}</span>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
    <div class="wrappen-header-block sidebar-content-block p-null">
        <div class="header-avatar">
            <div class="header-avatar-wrap"><img src="/dist/images/avatar.jpg" title="Avatar" alt="Avatar"/></div>
        </div>
    </div>
</header>