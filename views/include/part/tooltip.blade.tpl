@php $unique_id = rand(0, 1000) @endphp
<div class="tooltip">
    <div class="tooltip-label">
        <a href="javascript:void(0);">
            <label for="checkbox-title-{{ $unique_id }}">{{ $title }}</label>
        </a>
    </div>
    <input type="checkbox" id="checkbox-title-{{ $unique_id }}"/>
    <div class="tooltip-content">
        @includeIf('include.list.list-eval', ['items' => $evals])
    </div>
</div>