<!DOCTYPE html>
<html lang="ru">
    @include ('layouts.include.control-head')

    <body>
        <div class="control-area"></div>
        @if(!empty($langs))
            <div class="wrappen-lang">
                <ul>
                    @foreach($langs as $key => $item)
                        <li class="d-inline-block">
                            <a @if(!empty($lang) && $key != $lang)href="?lng={{ $key }}" @endif @if(!empty($lang) && $key == $lang)class="active" @endif title="{{ $key }} - {{ $item }}">{{ ucfirst($key) }}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="wrappen">
            @yield('content')
        </div>

        @include ('layouts.include.control-footer-script')
    </body>
</html>