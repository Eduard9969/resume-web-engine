<head>
    <meta charset="utf-8"/>

    <title>{{ $title }}</title>
    <meta name="description" content="" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <meta name="format-detection" content="telephone=no"/>
    <meta http-equiv="x-rim-auto-match" content="none"/>

    <link rel="stylesheet" href="/dist/css/style.css"/>
    <link rel="stylesheet" href="/dist/css/print.css" media="print"/>

    <link href="https://fonts.googleapis.com/css?family=Lato:400,700&amp;display=swap" rel="stylesheet"/>
</head>